const gdal = require('gdal');

try {
  const dataset = gdal.open('shape/stations.shp');
  const layer = dataset.layers.get(0);
  console.log(`gdal version: ${gdal.version}`);
  console.log(`number of features: ${layer.features.count()}`);
  console.log(`fields: ${layer.fields.getNames()}`);
  console.log(`extent: ${JSON.stringify(layer.extent)}`);
  console.log(`srs: ${(layer.srs ? layer.srs.toWKT() : 'null')}`);
}
catch (e) {
  console.error('Unable to open shape.');
}
